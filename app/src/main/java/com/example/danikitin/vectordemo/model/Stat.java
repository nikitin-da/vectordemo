package com.example.danikitin.vectordemo.model;

/**
 * @author Dmitrii Nikitin
 */
public class Stat {

    private long first = 0L;
    private long average = 0L;
    private long count = 0L;

    public void trackEvent(final long sample) {
        if (count == 0) {
            first = sample;
        }
        count++;
        average -= average / count;
        average += sample / count;
    }

    public long getFirst() {
        return first;
    }

    public long getAverage() {
        return average;
    }

    public long getCount() {
        return count;
    }
}
