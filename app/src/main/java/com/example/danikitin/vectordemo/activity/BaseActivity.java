package com.example.danikitin.vectordemo.activity;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * @author Dmitrii Nikitin
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
