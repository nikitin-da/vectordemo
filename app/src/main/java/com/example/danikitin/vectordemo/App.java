package com.example.danikitin.vectordemo;

import android.app.Application;

/**
 * @author Dmitrii Nikitin
 */
public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }
}
