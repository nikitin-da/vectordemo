package com.example.danikitin.vectordemo.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.danikitin.vectordemo.model.Mode;
import com.example.danikitin.vectordemo.R;
import com.example.danikitin.vectordemo.model.Stat;

/**
 * @author Dmitrii Nikitin
 */
public class StatActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setContentView(R.layout.activity_stat);

        final ViewGroup container = (ViewGroup) findViewById(R.id.stat_container);
        container.addView(newStatLayout(Mode.VECTOR, container));
        container.addView(newStatLayout(Mode.PNG, container));
    }

    private View newStatLayout(@NonNull final Mode mode, @NonNull final ViewGroup parent) {
        final View view = LayoutInflater.from(this).inflate(R.layout.layout_stat, parent, false);

        final boolean hasData = !mode.getImageStatMap().isEmpty();

        final String title;
        final String modeTitle = getString(mode.getTitleResId());
        if (hasData) {
            fillStat(mode, view);
            title = modeTitle;
        } else {
            title = getString(R.string.no_data_suffix, modeTitle);
        }
        ((TextView) view.findViewById(R.id.stat_mode_name)).setText(title);

        view.findViewById(R.id.stat_mode_data_container).setVisibility(hasData ? View.VISIBLE : View.GONE);
        return view;
    }

    private void fillStat(@NonNull final Mode mode, @NonNull final View statLayout) {
        Long minFirst = Long.MAX_VALUE;
        Long maxFirst = Long.MIN_VALUE;
        Long minAverage = Long.MAX_VALUE;
        Long maxAverage = Long.MIN_VALUE;

        for (Stat stat : mode.getImageStatMap().values()) {
            if (minFirst > stat.getFirst()) {
                minFirst = stat.getFirst();
            }
            if (maxFirst < stat.getFirst()) {
                maxFirst = stat.getFirst();
            }
            if (minAverage > stat.getAverage()) {
                minAverage = stat.getAverage();
            }
            if (maxAverage < stat.getAverage()) {
                maxAverage = stat.getAverage();
            }
        }
        ((TextView) statLayout.findViewById(R.id.common_first_min)).setText(getString(R.string.common_stat_min, minFirst));
        ((TextView) statLayout.findViewById(R.id.common_first_max)).setText(getString(R.string.common_stat_max, maxFirst));
        ((TextView) statLayout.findViewById(R.id.common_average_min)).setText(getString(R.string.common_stat_min, minAverage));
        ((TextView) statLayout.findViewById(R.id.common_average_max)).setText(getString(R.string.common_stat_max, maxAverage));
    }
}
