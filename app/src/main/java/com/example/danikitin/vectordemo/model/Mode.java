package com.example.danikitin.vectordemo.model;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.example.danikitin.vectordemo.App;
import com.example.danikitin.vectordemo.R;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitrii Nikitin
 */
public enum Mode {
    VECTOR(R.array.vector_images, R.string.vector_title),
    PNG(R.array.png_images, R.string.png_title);

    private final TypedArray images;
    private final Integer titleResId;
    private final Map<Integer, Stat> imageStatMap = new HashMap<>();

    Mode(@ArrayRes final Integer imageArrayResId, @StringRes final Integer titleResId) {
        this.titleResId = titleResId;
        final Resources resources = App.getInstance().getResources();
        images = resources.obtainTypedArray(imageArrayResId);
        if (images == null) {
            throw new IllegalArgumentException("Can't obtain typed array for id: " + imageArrayResId);
        }
    }

    @NonNull
    public TypedArray getImages() {
        return images;
    }

    @StringRes
    public Integer getTitleResId() {
        return titleResId;
    }

    @NonNull
    public Map<Integer, Stat> getImageStatMap() {
        return imageStatMap;
    }

    @NonNull
    public Stat trackEvent(@DrawableRes final Integer drawableResId, long spentTime) {
        Stat drawableStat = imageStatMap.get(drawableResId);
        if (drawableStat == null) {
            drawableStat = new Stat();
            imageStatMap.put(drawableResId, drawableStat);
        }
        drawableStat.trackEvent(spentTime);
        return drawableStat;
    }
}
