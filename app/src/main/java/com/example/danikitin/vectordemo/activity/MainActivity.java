package com.example.danikitin.vectordemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.danikitin.vectordemo.model.Mode;
import com.example.danikitin.vectordemo.R;
import com.example.danikitin.vectordemo.model.Stat;

import static com.example.danikitin.vectordemo.model.Mode.*;

public class MainActivity extends BaseActivity {

    private static final String STATE_LIST_VIEW = "STATE_LIST_VIEW";
    private static final String STATE_MODE = "STATE_MODE";

    private static final Mode DEFAULT_MODE = VECTOR;

    private ListView listView;
    private ImagesAdapter imagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        imagesAdapter = new ImagesAdapter(this);
        listView.setAdapter(imagesAdapter);

        Mode restoredMode = null;
        if (savedInstanceState != null) {
            final Parcelable listViewState = savedInstanceState.getParcelable(STATE_LIST_VIEW);
            listView.onRestoreInstanceState(listViewState);

            restoredMode = (Mode) savedInstanceState.getSerializable(STATE_MODE);
        }
        changeMode(restoredMode != null ? restoredMode : DEFAULT_MODE);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        final Mode currentMode = (imagesAdapter != null) ? imagesAdapter.getCurrentMode() : null;
        menu.findItem(R.id.action_vector).setVisible(currentMode == PNG);
        menu.findItem(R.id.action_png).setVisible(currentMode == VECTOR);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_vector:
                changeMode(VECTOR);
                break;
            case R.id.action_png:
                changeMode(PNG);
                break;
            case R.id.action_stat:
                startActivity(new Intent(this, StatActivity.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (imagesAdapter != null && imagesAdapter.getCurrentMode() != null) {
            outState.putSerializable(STATE_MODE, imagesAdapter.getCurrentMode());
        }
        if (listView != null) {
            outState.putParcelable(STATE_LIST_VIEW, listView.onSaveInstanceState());
        }
        super.onSaveInstanceState(outState);
    }

    private void changeMode(@NonNull final Mode mode) {
        imagesAdapter.setImages(mode);
        setTitle(mode.getTitleResId());
        supportInvalidateOptionsMenu();
    }

    public static class ImagesAdapter extends BaseAdapter {
        private final Context context;
        private Mode currentMode;

        public ImagesAdapter(Context context) {
            this.context = context;
        }

        public void setImages(@NonNull final Mode mode) {
            currentMode = mode;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return currentMode != null ? currentMode.getImages().length() : 0;
        }

        @Override
        public Integer getItem(int position) {
            return currentMode.getImages().getResourceId(position, -1);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Holder holder;
            final View view;
            if (convertView != null && convertView.getTag() instanceof Holder) {
                view = convertView;
                holder = (Holder) convertView.getTag();
            } else {
                view = LayoutInflater.from(context).inflate(R.layout.item_image, parent, false);
                holder = new Holder(view);
                view.setTag(holder);
            }
            holder.fillData(getItem(position));
            return view;
        }

        @Nullable
        public Mode getCurrentMode() {
            return currentMode;
        }

        private class Holder {

            private final ImageView imageView;
            private final TextView firstTextView;
            private final TextView timesTextView;
            private final TextView averageTextView;

            private Holder(@NonNull final View view) {
                this.imageView = (ImageView) view.findViewById(R.id.item_image);
                this.firstTextView = (TextView) view.findViewById(R.id.item_first_time);
                this.timesTextView = (TextView) view.findViewById(R.id.item_times);
                this.averageTextView = (TextView) view.findViewById(R.id.item_average_time);
            }

            public void fillData(@DrawableRes final Integer imageResId) {
                final long start = SystemClock.elapsedRealtime();
                imageView.setImageResource(imageResId);
                final long time = SystemClock.elapsedRealtime() - start;
                final Stat statistic = currentMode.trackEvent(imageResId, time);
                firstTextView.setText(context.getString(R.string.item_stat_first, statistic.getFirst()));
                timesTextView.setText(context.getString(R.string.item_stat_times, statistic.getCount()));
                averageTextView.setText(context.getString(R.string.item_stat_average, statistic.getAverage()));
            }
        }
    }
}
